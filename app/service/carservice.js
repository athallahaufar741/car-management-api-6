const carrepo = require("../repositories/carrepo");

module.exports = {
  create(requestBody) {
    return carrepo.create(requestBody);
  },

  update(id, requestBody) {
    return carrepo.update(id, requestBody);
  },

  delete(id) {
    return carrepo.delete(id);
  },

  get(id) {
    return carrepo.find(id);
  },
};
