const userrepo = require("../repositories/userrepo");

module.exports = {
  async create(requestBody) {
    return userrepo.create(requestBody);
  },

  async update(id, requestBody) {
    return userrepo.update(id, requestBody);
  },

  async delete(id) {
    return userrepo.delete(id);
  },

  async get(id) {
    return userrepo.find(id);
  },
};
