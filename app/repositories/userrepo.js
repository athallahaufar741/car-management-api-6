const { user } = require("../models");

module.exports = {
  create(createArgs) {
    return user.create(createArgs);
  },

  update(id, updateArgs) {
    return user.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return user.destroy({ where: { id } });
  },

  find(id) {
    return user.findByPk(id);
  },

  findAll() {
    return user.findAll();
  },
};
